package com.dosgi.kit;


/**
 * @author dingnate
 *
 */
public class PathKitTest extends PathKit {
	public static void main(String[] args) {
		System.out.println(getParent("a//b\\/c//\\"));
		System.out.println(getParent("a"));
		System.out.println(getParent(""));
		System.out.println(getParent("/a/b"));
		System.out.println(append("a//b\\/c//\\", "/a/b/"));
		System.out.println(append("/a/b/", ""));
		System.out.println(append("/a", "b"));
		System.out.println(append("/a/b/", "c"));
	}
}
