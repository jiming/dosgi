package com.dosgi.module;



/**
 * 模块抽象接口
 * @author dingnate
 *
 */
public interface IModule {
	/**
	 * 模块初始化入口，模块启动准备的逻辑请放在这儿。<br>
	 * 如：服务注册，类处理器添加等
	 * @param context
	 * @throws Exception
	 */
	public void init(IModuleContext context) throws Exception;
	/**
	 * 模块启动入口
	 * @param context
	 * @throws Exception
	 */
	public void start(IModuleContext context) throws Exception;
	
	/**
	 * 模块停止入口
	 * @param context
	 * @throws Exception
	 */
	public void stop(IModuleContext context) throws Exception;

}
