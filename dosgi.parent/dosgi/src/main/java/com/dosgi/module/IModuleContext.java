package com.dosgi.module;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface IModuleContext {
	/**
	 * 启动类
	 */
	static final String BOOT_CLASS = "Boot-Class";

	/**
	 * 获取模块的类加载器
	 * 
	 * @return
	 */
	public ClassLoader getClassLoader();

	/**
	 * 初始化模块
	 * 
	 * @throws Exception
	 */
	public void init() throws Exception;
	

	/**
	 * 扫描模块类
	 * 
	 * @throws Exception
	 */
	public void scanClasses()throws Exception;

	/**
	 * 启动模块
	 * 
	 * @throws Exception
	 */
	public void start() throws Exception;

	/**
	 * 停止模块
	 * 
	 * @throws Exception
	 */
	public void stop() throws Exception;

	/**
	 * 获取模块路径
	 * 
	 * @return
	 */
	public String getFilePath();

	/**
	 * 获取Manifest.MF文件信息
	 * 
	 * @return
	 */
	public Map<String, String> getManifestAttributes();

	/**
	 * 获取模块标识名称
	 * 
	 * @return
	 */
	public String getSymbolicName();

	/**
	 * 获取模块版本
	 * 
	 * @return
	 */
	public String getVersion();
	
	/**
	 * @return 模块标识名
	 */
	String getQualifiedName();

	/**
	 * @return 参与类扫描的类路径前缀
	 */
	String getClassPrefix();
	/**
	 * 通过类注册BeanDefinition，class为Key
	 * 
	 * @param clazz
	 * @param bean
	 */
	public void registry(Class<?> clazz, Object bean);

	/**
	 * 通过类获取已注册Bean
	 * 
	 * @param clazz
	 * @return
	 */
	public <T> T get(Class<T> clazz);

	/**
	 * 获取module的ExportPackages
	 * 
	 * @return
	 */
	public Set<String> getExportPackages();

	/**
	 * 获取module的ImportPackages
	 * 
	 * @return
	 */
	public Map<String, String> getImportPackages();
	
	/**
	 * 获取module的RequireModules
	 * 
	 * @return
	 */
	public Map<String, String> getRequireModules();
	
	/**
	 * @return the 获取RequireModules声明的模块
	 */
	public Collection<IModuleContext> getResolvedRequireModules();
	
	/**
	 * @return the 获取RequireModules声明和ImportPackages声明的模块
	 */
	public Collection<IModuleContext> getResolvedRequireModulesWithImport();
	
	String getPhase();

	/**
	 * 获取直接依赖此模块的模块集合
	 * 
	 * @return
	 */
	Collection<IModuleContext> getReferenceModules();

	/**
	 * 其它模块通知依赖了此模块
	 * 
	 * @param mContext
	 */
	void addReferenceModule(IModuleContext mContext);
}
