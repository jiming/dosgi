package com.dosgi;

import java.util.Set;

import com.dosgi.bean.IBeanFactory;
import com.dosgi.clazz.ClassHandler;
import com.dosgi.module.IModuleContext;

/**
 * 应用上下文
 * @author dingnate
 *
 */
public interface IDosgiContext {
	/**
	 * 启动上下文
	 * @return
	 */
	public void start();

	/**
	 * 启动上下文
	 * @param standalone 独立应用
	 */
	void start(boolean standalone);

	/**
	 * 停止上下文
	 */
	public void stop();

	/**
	 * 启动模块
	 * 
	 * @param symbolicName
	 * @param version
	 */
	void startModule(String symbolicName, String version);

	/**
	 * 重启模块
	 * @param symbolicName
	 * @param version
	 */
	void restartModule(String symbolicName, String version);

	/**停止模块
	 * @param symbolicName
	 * @param version
	 */
	void stopModule(String symbolicName, String version);

	/**
	 * Bean工厂
	 * @return
	 */
	public IBeanFactory beanFactory();

	/**
	 * 注册module
	 * 
	 * @param mooduleContext
	 */
	void registerModule(IModuleContext mooduleContext);
	
	/**
	 * 获取module
	 * 
	 * @param symbolicName
	 * @param version
	 * @return
	 */
	public IModuleContext getModule(String symbolicName, String version);

	/**
	 * 通过ExportPackage获取module
	 * 
	 * @param packageName
	 * @param version
	 * @return
	 */
	public IModuleContext getModuleByExportPackage(String packageName, String version);

	/**
	 * 添加类处理器
	 * @param classHandler
	 */
	public void addClassHandler(ClassHandler classHandler);

	/**
	 * 获取所有的已添加的类处理器
	 * 
	 * @return
	 */
	public Set<ClassHandler> getClassHandlers();

	/**
	 * @return 模块主目录
	 */
	String getModuleHome();

	/**
	 * 启动模块
	 * 
	 * @param moduleJar
	 */
	void startModule(String moduleJar);

	/**
	 * 停止模块
	 * 
	 * @param moduleJar
	 */
	void stopModule(String moduleJar);
}
