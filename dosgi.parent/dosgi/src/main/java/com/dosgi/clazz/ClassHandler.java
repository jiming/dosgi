package com.dosgi.clazz;

import com.dosgi.module.IModuleContext;


public abstract class ClassHandler {
	protected IModuleContext mContext;

	protected ClassHandler(IModuleContext mContext) {
		this.mContext = mContext;
	}

	public abstract void handle(Class<?> clazz) throws Exception;
}
