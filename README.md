# dosgi（道斯基）

## 简介

dosgi（道斯基）是基于osgi实现的模块框架，如果你想做模块化开发（部署）又嫌Equinox和Felix笨重，那你可以试试dosgi。

## MANIFEST.MF规范

#### 1.配置pom文件打包插件

```xml
<build>                                                
	<plugins>                                          
		<plugin>                                       
			<groupId>org.apache.maven.plugins</groupId>
			<artifactId>maven-jar-plugin</artifactId>  
			<configuration>                            
				<archive>                              
					<manifestEntries>                  
```
#### 2.manifestEntries节点下的MANIFEST.MF生成规范如下

```xml
<!-- 下面为模块依赖项 -->
<!-- 作为模块名，必选 -->
<Implementation-Title>${project.name}</Implementation-Title>
<!-- 作为模块版本,必选 -->
<Implementation-Version>${project.version}</Implementation-Version>
<!-- 模块的启动类，需要实现com.dosgi.module.IModule接口,必选  -->
<Boot-Class>dosgi.module.system.SystemModule</Boot-Class>
<!-- System模块的标识，System模块唯一，会首先启动，可选 -->
<Phase>System</Phase>
<!-- 框架只会扫描Module-Class-Prefix指定路径下面的class,,可选 -->
<Module-Class-Prefix>${module.class.root}</Module-Class-Prefix>
<!-- 导出包声明，多个声明已“,”分割 ，只会导出包根目录下的类,可选 -->
<!-- <Export-Package>com.x,com.x.y</Export-Package> -->
<!-- 导入包声明，多个声明已“,”分割,可指定version，可访问导入包根目录下的类 ,可选-->
<!-- <Import-Package>com.x.y,com.x;version=1.2.3</Import-Package> -->
<!-- 依赖module声明，多个声明已“,”分割,可指定version，可访问依赖module下的类,可选 -->
<!-- <Require-Module>module12,module11;version=1.2.3</Require-Module> -->
<!-- 下面为非模块依赖项 -->
<Specification-Title>${project.name}</Specification-Title>
<Specification-Version>${project.version}</Specification-Version>
<Specification-Vendor>${project.organization.name}</Specification-Vendor>
<Implementation-Vendor>${project.organization.name}</Implementation-Vendor>
<Implementation-Vendor-Id>dosgi</Implementation-Vendor-Id>
<X-Compile-Source-JDK>${maven.compile.source}</X-Compile-Source-JDK>
<X-Compile-Target-JDK>${maven.compile.target}</X-Compile-Target-JDK>
```
## 使用方法

### 普通应用

#### 开发

- 下载工程代码
- 新建模块工程，可以参见dosgi.module.system工程的SystemModule和pom文件的打包配置
- 开发模块业务逻辑
- 运行dosgi.parent的pom.xml打包
- 通过com.dosgi.Dosgi.start()类运行调式

#### 部署

- 运行dosgi.parent的pom.xml打包
- 参考附件**部署dosgi的例子**将包放入modules目录
- 根据需要修改启动脚本，运行启动脚本


### web应用

#### 开发

- 下载工程代码
- 新建模块工程，可以参见dosgi.module.system工程的SystemModule和pom文件的打包配置
- 开发模块业务逻辑，开发web逻辑
- 修改web.xml中的module.home为绝对路径，调式：*dosgi.web->run as->run on server->tomcat*

#### 部署

- 切换web.xml中的module.home为部署路径路
- 运行dosgi.parent的pom.xml打包
- 复制*dosgi.parent/m*odules文件夹到*tomcat/webapp/* 路径下
- 运行tomcat

## 更新日志

2018/04/04

- dosgi模块工程支持maven坐标依赖外部库
- 支持模块动态启动，停止，重启
- 小范围重构：DosgiLauncher,DosgiContextDefault,Dosgi,DosgiClassLoader

2018/04/03

- 增加dosgi.web工程，支持web开发
- 删除Main类，修改入口为com.dosgi.Dosgi.start()

## 交流

请在下方评论或提issue，我会及时回复。

## 打赏

如果感觉本项目对您有用，打赏是对作者最大的鼓励。

![](https://note.youdao.com/yws/api/personal/file/9C97D8BD1E9E497097DEC70A1E739090?method=download&shareKey=90d5b78980e32ff73fde6c3035e34a75)